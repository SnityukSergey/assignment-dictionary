section .text
extern string_equals
global find_word

find_word:
    push rsi
    push rdi
    add rsi, 8
    call string_equals
    pop rdi
    pop rsi
    cmp rax, 0
    jnz .success
    
    mov rsi, [rsi]
    cmp rsi, 0
    jz .fail
    jmp find_word

    .success:
        mov rax, rsi
        ret

    .fail:
        mov rax, 0
        ret
