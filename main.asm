%include "colon.inc"
%include "words.inc"

extern exit
extern string_length
extern print_string
extern print_char
extern print_newline
extern print_uint
extern print_int
extern string_equals
extern read_char
extern read_word
extern parse_uint
extern parse_int
extern string_copy
extern print_error
extern find_word

global _start

section .data
    buff: times 255 db 0
    wrong_key: db "Wrong key", 0xA, 0
    wrong_length: db "Wrong key length", 0xA, 0

section .text

_start:
    mov rdi, buff
    mov rsi, 255
    call read_word
    cmp rax, 0
    jz .wrong_length_err
    mov rdi, rax
    mov rsi, next
    call find_word
    cmp rax, 0
    jz .wrong_key_err
    add rax, 8
    mov rdi, rax
    call string_length
    add rdi, rax
    inc rdi
    call print_string
    call exit

    .wrong_length_err:
        mov rdi, wrong_length
        call print_error
        call exit

    .wrong_key_err:
        mov rdi, wrong_key
        call print_error
        call exit
